Twitter API testing with Spock Framework
===============================

Building with Gradle
--------------------
Type:

    ./gradlew clean test
    ./gradlew spockTest

Report generated in /build/reports/tests/index.html

